# Dius Tennis Match

The test link [here](https://github.com/DiUS/coding-tests/blob/master/dius_tennis.md)

## 1. Code Architecture
The `Match` class is a **God** class that includes internal initiations of three dependencies; `Point`, `Game`, and `Set` score managers. According to the clean code architecture, it is not recommended to initiate dependencies inside of the class. However, in order to represent the following functionalities, as shown in the test link, I presume the `Match` can be easily updated if internal dependency implementation is changed.

```bash
const match = new Match('Player 1', 'Player 2');
match.pointWonBy('Player 1');
match.score();
```
![](./imgs/arch.png)

Tennis match include three score systems,
- point scores
- game scores
- set scores

Basically, if a player wins each score system, the upper rank score is accumulated. However, scoring do not always trigger events from lower to upper systems. For example, the point system adds a game score if the player gets the point after forty or advantage in normal, but if the current game condition moves to tie-break, then this condition affects to the point score system.

![](./imgs/scoring.png)

Thus, basically, the lower score system should include the its upper score dependency, in order to add upper scores at each win.

## 2. How to Use

### 2.1. NPM
The version of Node is `14.17.31`. If you use `nvm`, simply execute `nvm use` to change the target node version.
```bash
# if nvm is used,
nvm use

# install packages
npm ci or npm i -q
```

### 2.2. Example run

`src/index.ts` includes an example scenario of the match. It can be triggered by executing the following command,

```bash
npm run index
``` 

### 2.3. Unit test

The `test` directory includes three score manager unit tests. Each score system's upper score dependency is mocked by the jest framework, in order to focus on the test subject itself.

```bash
npm run test
```
with coverage,
```bash
npm run test:coverage
```
