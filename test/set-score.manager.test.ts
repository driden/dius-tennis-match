import SetScoreManager from "../src/implementation/set-score.manager";
import ISetScoreManager from "../src/interface/set-score.manager.i";


describe("Set Score Manager TEST", () => {
	const player1Name = "Red";
	const player2Name = "Blue";
	let setScoreManager: ISetScoreManager;
	let updateToSpy;
	beforeEach(() => {
		setScoreManager = new SetScoreManager(player1Name, player2Name);
		updateToSpy = jest.spyOn(setScoreManager, "updateTo");
	});
	afterEach(() => {
		jest.resetAllMocks();
	});
	it("should update the Player 1 set score from [updateTo]", () => {
		setScoreManager.updateTo(player1Name);
		expect(updateToSpy).toBeCalledTimes(1);
	});
	it("should throw an error if more than 1 set score added from [updateTo]", () => {
		expect(()=>Array.of(1, 2).forEach(() => setScoreManager.updateTo(player2Name)))
			.toThrow('The Match is over');
		expect(updateToSpy).toBeCalledTimes(2);
	});
	it("should throw an error if reset", () => {
		expect(()=>setScoreManager.reset()).toThrowError();
	});
	it("should return the string value from [result]", async () => {
		expect(typeof setScoreManager.result()).toBe('string');
	});
	it("should return the string value from [getCondition]", () => {
		expect(typeof setScoreManager.getCondition()).toBe('string');
	});
	it("should throw an error if wrong player name is added to [updateTo]", () => {
		expect(()=>setScoreManager.updateTo("green")).toThrowError();
	});
});