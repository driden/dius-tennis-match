import GameScoreManager from "../src/implementation/game-score.manager";
import { GameConditionEnum } from "../src/interface/enum";
import IGameScoreManager from "../src/interface/game-score.manager.i";
import ISetScoreManager from "../src/interface/set-score.manager.i";




describe("Set Score Manager TEST", () => {
	const player1Name = "Red";
	const player2Name = "Blue";
	let gameScoreManager: IGameScoreManager;
	const mockSetScoreManger: jest.Mocked<ISetScoreManager> = {
		reset: jest.fn(),
		result: jest.fn(),
		updateTo: jest.fn(),
		getCondition: jest.fn()
	};
	beforeEach(() => {
		gameScoreManager = new GameScoreManager(player1Name, player2Name, mockSetScoreManger);
	});
	afterEach(() => {
		jest.resetAllMocks();
	});
	it("should update the Player 1 game score from [updateTo]", () => {
		const updateToSpy = jest.spyOn(gameScoreManager, "updateTo");
		gameScoreManager.updateTo(player1Name);
		expect(updateToSpy).toBeCalledTimes(1);
		updateToSpy.mockClear();
	});
	it("should trigger [SetScoreManager.updateTo] when the Player 1 wins a set from [updateTo]", () => {
		Array.of(1,2,3,4,5,6).forEach(() => gameScoreManager.updateTo(player1Name));
		expect(mockSetScoreManger.updateTo).toBeCalledTimes(1);
	});
	it("should trigger the [reset] function when the Player 1 wins a set from [updateTo]", () => {
		const resetSpyOn = jest.spyOn(gameScoreManager, "reset");
		Array.of(1,2,3,4,5,6).forEach(() => gameScoreManager.updateTo(player1Name));
		expect(resetSpyOn).toBeCalledTimes(1);
		resetSpyOn.mockClear();
	});
	it("should trigger the [reset] function when the Player 2 wins a set from [updateTo]", () => {
		const resetSpyOn = jest.spyOn(gameScoreManager, "reset");
		Array.of(1,2,3,4,5,6).forEach(() => gameScoreManager.updateTo(player2Name));
		expect(resetSpyOn).toBeCalledTimes(1);
		resetSpyOn.mockClear();
	});
	it("should return tie-break condition from [getCondition] on 6 : 6", () => {
		const resetSpyOn = jest.spyOn(gameScoreManager, "reset");
		Array.of(1,2,3,4,5).forEach(() => gameScoreManager.updateTo(player1Name));
		Array.of(1,2,3,4,5).forEach(() => gameScoreManager.updateTo(player2Name));
		gameScoreManager.updateTo(player1Name);
		gameScoreManager.updateTo(player2Name);
		expect(gameScoreManager.getCondition()).toBe(GameConditionEnum.TIEBREAK);
		resetSpyOn.mockClear();
	});
	it("should return the string value from [result]", async () => {
		expect(typeof gameScoreManager.result()).toBe('string');
	});
	it("should return the string value from [getCondition]", () => {
		expect(typeof gameScoreManager.getCondition()).toBe('string');
	});
	it("should throw an error if wrong player name is added to [updateTo]", () => {
		expect(()=>gameScoreManager.updateTo("green")).toThrowError();
	});
});