import IGameScoreManager from "../src/interface/game-score.manager.i";
import IPointStoreManager from "../src/interface/point-score.manager.i";
import PointScoreManager from "../src/implementation/point-score.manager";
import { GameConditionEnum, PointConditionEnum } from "../src/interface/enum";


describe("Set Score Manager TEST", () => {
	const player1Name = "Red";
	const player2Name = "Blue";
	let pointScoreManager: IPointStoreManager;
	const mockGameScoreManger: jest.Mocked<IGameScoreManager> = {
		reset: jest.fn(),
		result: jest.fn(),
		updateTo: jest.fn(),
		getCondition: jest.fn()
	};
	
	beforeEach(() => {
		pointScoreManager = new PointScoreManager(player1Name, player2Name, mockGameScoreManger);
	});
	afterEach(() => {
		jest.resetAllMocks();
	});
	it("should update the Player 1 point score from [updateTo]", () => {
		const updateToSpy = jest.spyOn(pointScoreManager, "updateTo");
		pointScoreManager.updateTo(player1Name);
		expect(updateToSpy).toBeCalledTimes(1);
		updateToSpy.mockClear();
	});
	it("should trigger [GameScoreManager.updateTo] when the Player 1 wins a game from [updateTo]", () => {
		Array.of(1,2,3,4).forEach(() => pointScoreManager.updateTo(player1Name));
		expect(mockGameScoreManger.updateTo).toBeCalledTimes(1);
	});
	it("should trigger [GameScoreManager.updateTo] when the Player 2 wins a game from [updateTo]", () => {
		Array.of(1,2,3,4).forEach(() => pointScoreManager.updateTo(player2Name));
		expect(mockGameScoreManger.updateTo).toBeCalledTimes(1);
	});
	it("should trigger the [reset] function when the Player 1 wins a set from [updateTo]", () => {
		const resetSpyOn = jest.spyOn(pointScoreManager, "reset");
		Array.of(1,2,3,4).forEach(() => pointScoreManager.updateTo(player1Name));
		expect(resetSpyOn).toBeCalledTimes(1);
		resetSpyOn.mockClear();
	});
	it("should trigger [GameScoreManager.updateTo] when the Player 1 wins a game from [updateTo] with Tie-break condition", () => {
		mockGameScoreManger.getCondition = jest.fn(() => GameConditionEnum.TIEBREAK);
		Array.of(1,2,3,4, 5, 6, 7).forEach(() => pointScoreManager.updateTo(player1Name));
		expect(mockGameScoreManger.updateTo).toBeCalledTimes(1);
	});
	it("should trigger the [reset] function when the Player 1 wins a set from from [updateTo] with Tie-break condition", () => {
		mockGameScoreManger.getCondition = jest.fn(() => GameConditionEnum.TIEBREAK);
		const resetSpyOn = jest.spyOn(pointScoreManager, "reset");
		Array.of(1,2,3,4, 5, 6, 7).forEach(() => pointScoreManager.updateTo(player1Name));
		expect(resetSpyOn).toBeCalledTimes(1);
		resetSpyOn.mockClear();
	});
	it("should trigger [GameScoreManager.updateTo] when the Player 2 wins a game from [updateTo] with Tie-break condition", () => {
		mockGameScoreManger.getCondition = jest.fn(() => GameConditionEnum.TIEBREAK);
		Array.of(1,2,3,4, 5, 6, 7).forEach(() => pointScoreManager.updateTo(player2Name));
		expect(mockGameScoreManger.updateTo).toBeCalledTimes(1);
	});
	it("should trigger the [reset] function when the Player 2 wins a set from from [updateTo] with Tie-break condition", () => {
		mockGameScoreManger.getCondition = jest.fn(() => GameConditionEnum.TIEBREAK);
		const resetSpyOn = jest.spyOn(pointScoreManager, "reset");
		Array.of(1,2,3,4, 5, 6, 7).forEach(() => pointScoreManager.updateTo(player2Name));
		expect(resetSpyOn).toBeCalledTimes(1);
		resetSpyOn.mockClear();
	});
	it("should return the non-tie break point score from [result]", async () => {
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player1Name));
		expect(pointScoreManager.result()).toBe('40-0');
	});
	it("should return the tie break point score from [result]", async () => {
		mockGameScoreManger.getCondition = jest.fn(() => GameConditionEnum.TIEBREAK);
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player1Name));
		expect(pointScoreManager.result()).toBe('3-0');
	});
	it("should return 'deuce' from [result] when the Player 1 gets the point", () => {
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player1Name));
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player2Name));
		expect(pointScoreManager.result()).toBe('deuce');
	});
	it("should return 'DEUCE' from [getCondition] when the Player 1 gets the point", () => {
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player1Name));
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player2Name));
		expect(pointScoreManager.getCondition()).toBe(PointConditionEnum.DEUCE);
	});
	it("should return 'Advantage to {Player 1}' from [result] when the Player 1 gets the point after deuce", () => {
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player1Name));
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player2Name));
		pointScoreManager.updateTo(player1Name);
		expect(pointScoreManager.result()).toContain(player1Name);
	});
	it("should return 'P1AD' from [getCondition] when the Player 1 gets the point after deuce", () => {
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player1Name));
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player2Name));
		pointScoreManager.updateTo(player1Name);
		expect(pointScoreManager.getCondition()).toBe(PointConditionEnum.P1AD);
	});
	it("should return 'Advantage to {Player 2}' from [result] when the Player 2 gets the point after deuce", () => {
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player1Name));
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player2Name));
		pointScoreManager.updateTo(player2Name);
		expect(pointScoreManager.result()).toContain(player2Name);
	});
	it("should return 'P2AD' from [getCondition] when the Player 2 gets the point after deuce", () => {
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player1Name));
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player2Name));
		pointScoreManager.updateTo(player2Name);
		expect(pointScoreManager.getCondition()).toBe(PointConditionEnum.P2AD);
	});
	it("should trigger [GameScoreManager.updateTo] when the Player 1 wins a game after Advantage to {Player 1}", () => {
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player1Name));
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player2Name));
		pointScoreManager.updateTo(player1Name);
		pointScoreManager.updateTo(player1Name);
		expect(mockGameScoreManger.updateTo).toBeCalledTimes(1);
	});
	it("should trigger [GameScoreManager.updateTo] when the Player 2 wins a game after Advantage to {Player 2}", () => {
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player1Name));
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player2Name));
		pointScoreManager.updateTo(player2Name);
		pointScoreManager.updateTo(player2Name);
		expect(mockGameScoreManger.updateTo).toBeCalledTimes(1);
	});
	it("should return 'DEUCE' from [getCondition] when the Player 1 gets the point after Advantage to {Player 2}", () => {
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player1Name));
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player2Name));
		pointScoreManager.updateTo(player2Name);
		pointScoreManager.updateTo(player1Name);
		expect(pointScoreManager.getCondition()).toBe(PointConditionEnum.DEUCE);
	});
	it("should return 'DEUCE' from [getCondition] when the Player 2 gets the point after Advantage to {Player 1}", () => {
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player1Name));
		Array.of(1,2,3).forEach(() => pointScoreManager.updateTo(player2Name));
		pointScoreManager.updateTo(player1Name);
		pointScoreManager.updateTo(player2Name);
		expect(pointScoreManager.getCondition()).toBe(PointConditionEnum.DEUCE);
	});
	it("should throw an error if wrong player name is added to [updateTo]", () => {
		expect(()=>pointScoreManager.updateTo("green")).toThrowError();
	});
});