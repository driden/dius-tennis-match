import Match from "./implementation/match";
import IMatch from "./interface/match.i";

const player1Name = "Red";
const player2Name = "Blue";

const match1: IMatch = new Match(player1Name, player2Name);

// Check Deuce and Advantage
console.log("........................Deuce and Advantage");
const scenario1 = [
	player1Name,
	player1Name,
	player1Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player1Name,
	player1Name,
	player1Name,
];

scenario1.forEach((player) => {
	match1.pointWonBy(player);
	match1.score();
});
// Check game count
const scenario2 = [player1Name, player1Name, player1Name, player2Name, player2Name, player1Name];

scenario2.forEach((player) => {
	match1.pointWonBy(player);
});

const scenario3 = [
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
];

scenario3.forEach((player) => {
	match1.pointWonBy(player);
});

const scenario4 = [
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
];

scenario4.forEach((player) => {
	match1.pointWonBy(player);
});

console.log("........................Win a set to " + player2Name);

const scenario5 = [player2Name, player2Name, player2Name, player2Name];

scenario5.forEach((player) => {
	match1.pointWonBy(player);
	match1.score();
});

console.log("........................Before Tie-break");

const match2: IMatch = new Match(player1Name, player2Name);
const scenario6 = [
	player1Name,
	player1Name,
	player1Name,
	player1Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player1Name,
	player1Name,
	player1Name,
	player1Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player1Name,
	player1Name,
	player1Name,
	player1Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player1Name,
	player1Name,
	player1Name,
	player1Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player1Name,
	player1Name,
	player1Name,
	player1Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player1Name,
	player1Name,
	player1Name,
	player1Name,
	player2Name,
	player2Name,
	player2Name,
];

scenario6.forEach((player) => {
	match2.pointWonBy(player);
});
match2.score();

console.log("........................Tie-break point count 1");
const scenario7 = [
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
];
scenario7.forEach((player) => {
	match2.pointWonBy(player);
	match2.score();
});
console.log("........................Tie-break point count 2");
const scenario8 = [
	player1Name,
	player1Name,
	player1Name,
	player1Name,
	player1Name,
	player1Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player1Name,
	player2Name,
	player1Name,
	player2Name,
	player1Name,
	player2Name,
	player1Name,
	player2Name,
	player1Name,
	player1Name,
];
scenario8.forEach((player) => {
	match2.pointWonBy(player);
	match2.score();
});
console.log("........................Tie-break point count 3");
const scenario9 = [
	player1Name,
	player1Name,
	player1Name,
	player1Name,
	player1Name,
	player1Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player1Name,
	player2Name,
	player1Name,
	player2Name,
	player1Name,
	player2Name,
	player1Name,
	player2Name,
	player1Name,
	player1Name,
];
scenario9.forEach((player) => {
	match2.pointWonBy(player);
	match2.score();
});
console.log("........................Win a set to " + player1Name);
const scenario10 = [
	player1Name,
	player1Name,
	player1Name,
	player1Name,
	player1Name,
	player1Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player2Name,
	player1Name,
	player2Name,
	player1Name,
	player2Name,
	player1Name,
	player2Name,
	player1Name,
	player2Name,
	player1Name,
	player1Name,
];
scenario10.forEach((player) => {
	match2.pointWonBy(player);
	match2.score();
});
