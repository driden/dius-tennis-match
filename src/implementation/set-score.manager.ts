import { SetConditionEnum } from "../interface/enum";
import ISetScoreManager from "../interface/set-score.manager.i";

export default class SetScoreManager implements ISetScoreManager {
	private setCondition: SetConditionEnum = SetConditionEnum.NONE;
	private setMap: { [key: string]: number } = {};
	private readonly MAX_SET_NUM = 1; // only one set is allowed in this use case

	constructor(private player1Name: string, private player2Name: string) {
		this.setMap[player1Name] = 0;
		this.setMap[player2Name] = 0;
	}
	/**
	 * update a set score, set conditions
	 * since we only accept one set, the rule is simple
	 * @param playerName
	 * @returns
	 */
	updateTo(playerName: string): void {
		if (playerName !== this.player1Name && playerName !== this.player2Name) {
			throw new Error(`the player name: ${playerName} is not registered`);
		}
		let player1Set = this.setMap[this.player1Name];
		let player2Set = this.setMap[this.player2Name];
		if (player1Set + player2Set === this.MAX_SET_NUM) {
			throw new Error("The Match is over");
		}
		this.setMap[playerName] += 1;
		player1Set = this.setMap[this.player1Name];
		player2Set = this.setMap[this.player2Name];
		// actual tennis has 7 or 5 sets
		if (player1Set + player2Set === this.MAX_SET_NUM) {
			// Only consider who won the first set in this use case
			if (player1Set > player2Set) {
				this.setCondition = SetConditionEnum.P1MATCH;
				return;
			}
			if (player2Set > player1Set) {
				this.setCondition = SetConditionEnum.P2MATCH;
				return;
			}
		}
	}
	/**
	 * return the set score, however this won't be used in this use case
	 * @returns
	 */
	result(): string {
		const player1Set = this.setMap[this.player1Name];
		const player2Set = this.setMap[this.player2Name];
		return `${player1Set}-${player2Set}`;
	}
	/**
	 * no reset function is required
	 */
	reset(): void {
		throw new Error("The set cannot be reset");
	}
	/**
	 * return the set condition
	 * @returns
	 */
	getCondition(): SetConditionEnum {
		return this.setCondition;
	}
}
