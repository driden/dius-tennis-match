import { GameConditionEnum, PointConditionEnum } from "../interface/enum";
import IGameScoreManager from "../interface/game-score.manager.i";
import IPointStoreManager from "../interface/point-score.manager.i";

/**
 * score converting for non-tie-break points
 */
const scoreMap = {
	0: "0",
	1: "15",
	2: "30",
	3: "40",
};

export default class PointScoreManager implements IPointStoreManager {
	private pointCondition: PointConditionEnum = PointConditionEnum.NONE;
	private pointMap: { [key: string]: number } = {};
	constructor(private player1Name: string, private player2Name: string, private gameScoreManager: IGameScoreManager) {
		this.pointMap[player1Name] = 0;
		this.pointMap[player2Name] = 0;
	}
	/**
	 * Reset the current point
	 */
	reset(): void {
		this.pointMap[this.player1Name] = 0;
		this.pointMap[this.player2Name] = 0;
		this.pointCondition = PointConditionEnum.NONE;
	}
	/**
	 * return the point condition
	 * @returns
	 */
	getCondition(): PointConditionEnum {
		return this.pointCondition;
	}
	/**
	 * return the current point scores
	 * @returns
	 */
	result(): string {
		if (this.pointCondition === PointConditionEnum.DEUCE) return "deuce";
		if (this.pointCondition === PointConditionEnum.P1AD) return `Advantage to ${this.player1Name}`;
		if (this.pointCondition === PointConditionEnum.P2AD) return `Advantage to ${this.player2Name}`;
		const player1Point = this.pointMap[this.player1Name];
		const player2Point = this.pointMap[this.player2Name];
		if (this.gameScoreManager.getCondition() === GameConditionEnum.TIEBREAK) {
			return `${player1Point}-${player2Point}`;
		} else {
			return `${scoreMap[player1Point]}-${scoreMap[player2Point]}`;
		}
	}
	/**
	 * private function to update tie-break points
	 * @param playerName
	 * @returns
	 */
	private tieBreakUpdateTo(playerName: string): void {
		// add point
		this.pointMap[playerName] += 1;
		const player1Point = this.pointMap[this.player1Name];
		const player2Point = this.pointMap[this.player2Name];
		// 7 upward + 2 apart win the game
		if (player1Point >= 7 && player1Point - player2Point >= 2) {
			this.gameScoreManager.updateTo(this.player1Name);
			this.reset();
			return;
		}
		if (player2Point >= 7 && player2Point - player1Point >= 2) {
			this.gameScoreManager.updateTo(this.player2Name);
			this.reset();
			return;
		}
	}
	/**
	 * private function to update non-tie-break points
	 * @param playerName
	 * @returns
	 */
	private nonTieBreakUpdateTo(playerName: string): void {
		// if previously deuce, update to advantage
		if (this.pointCondition === PointConditionEnum.DEUCE) {
			if (playerName === this.player1Name) {
				this.pointCondition = PointConditionEnum.P1AD;
				return;
			}
			if (playerName === this.player2Name) {
				this.pointCondition = PointConditionEnum.P2AD;
				return;
			}
		}
		// if previously advantages, update game or deuce
		if (this.pointCondition === PointConditionEnum.P1AD) {
			if (playerName === this.player1Name) {
				this.gameScoreManager.updateTo(this.player1Name);
				this.pointCondition = PointConditionEnum.NONE;
				this.reset();
				return;
			}
			if (playerName === this.player2Name) {
				this.pointCondition = PointConditionEnum.DEUCE;
				return;
			}
		}
		if (this.pointCondition === PointConditionEnum.P2AD) {
			if (playerName === this.player2Name) {
				this.gameScoreManager.updateTo(this.player2Name);
				this.pointCondition = PointConditionEnum.NONE;
				this.reset();
				return;
			}
			if (playerName === this.player1Name) {
				this.pointCondition = PointConditionEnum.DEUCE;
				return;
			}
		}
		// add point if no condition matched
		this.pointMap[playerName] += 1;

		// update the point condition
		const player1Point = this.pointMap[this.player1Name];
		const player2Point = this.pointMap[this.player2Name];
		// update to deuce
		if (player1Point === 3 && player2Point === 3) {
			this.pointCondition = PointConditionEnum.DEUCE;
			return;
		}
		// update game score
		if (player1Point === 4) {
			this.gameScoreManager.updateTo(this.player1Name);
			this.reset();
			return;
		}
		if (player2Point === 4) {
			this.gameScoreManager.updateTo(this.player2Name);
			this.reset();
			return;
		}
	}
	/**
	 * update a point to the target player
	 * @param playerName
	 */
	updateTo(playerName: string): void {
		if (playerName !== this.player1Name && playerName !== this.player2Name) {
			throw new Error(`the player name: ${playerName} is not registered`);
		}
		// Tie-break point increase differently
		if (this.gameScoreManager.getCondition() === GameConditionEnum.TIEBREAK) {
			this.tieBreakUpdateTo(playerName);
		} else {
			this.nonTieBreakUpdateTo(playerName);
		}
	}
}
