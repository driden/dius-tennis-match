/* eslint-disable no-console */
import { SetConditionEnum } from "../interface/enum";
import IGameScoreManager from "../interface/game-score.manager.i";
import IMatch from "../interface/match.i";
import IPointStoreManager from "../interface/point-score.manager.i";
import ISetScoreManager from "../interface/set-score.manager.i";
import GameScoreManager from "./game-score.manager";
import PointScoreManager from "./point-score.manager";
import SetScoreManager from "./set-score.manager";
/**
 * Match Class
 * include three sub classes, Point, Game, Set Score
 */
export default class Match implements IMatch {
	private pointScoreManager: IPointStoreManager;
	private gameScoreManager: IGameScoreManager;
	private setScoreManager: ISetScoreManager;
	constructor(private player1Name: string, private player2Name: string) {
		this.setScoreManager = new SetScoreManager(player1Name, player2Name);
		this.gameScoreManager = new GameScoreManager(player1Name, player2Name, this.setScoreManager);
		this.pointScoreManager = new PointScoreManager(player1Name, player2Name, this.gameScoreManager);
	}
	/**
	 * adding a point to the player update point / game / set score gradually
	 * @param playerName
	 */
	pointWonBy(playerName: string): void {
		if (
			this.setScoreManager.getCondition() === SetConditionEnum.P1MATCH ||
			this.setScoreManager.getCondition() === SetConditionEnum.P2MATCH
		) {
			console.log("The match is over, no point update is allowed");
			return;
		}
		this.pointScoreManager.updateTo(playerName);
	}
	/**
	 * print the total score results
	 */
	score(): void {
		// only print the set winner
		if (this.setScoreManager.getCondition() === SetConditionEnum.P1MATCH) {
			console.log(`${this.player1Name} won the set`);
			return;
		}
		if (this.setScoreManager.getCondition() === SetConditionEnum.P2MATCH) {
			console.log(`${this.player2Name} won the set`);
			return;
		}
		// if match is still on going, print game and pont scores
		const message = `${this.gameScoreManager.result()}, ${this.pointScoreManager.result()}`;
		console.log(message);
	}
}
