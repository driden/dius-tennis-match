import { GameConditionEnum } from "../interface/enum";
import IGameScoreManager from "../interface/game-score.manager.i";
import ISetScoreManager from "../interface/set-score.manager.i";

export default class GameScoreManager implements IGameScoreManager {
	private gameCondition: GameConditionEnum = GameConditionEnum.NONE;
	private gameMap: { [key: string]: number } = {};
	constructor(private player1Name: string, private player2Name: string, private setScoreManager: ISetScoreManager) {
		this.gameMap[player1Name] = 0;
		this.gameMap[player2Name] = 0;
	}
	/**
	 * update conditions and game scores, and set score if won a set
	 * @param playerName
	 * @returns
	 */
	updateTo(playerName: string): void {
		if (playerName !== this.player1Name && playerName !== this.player2Name) {
			throw new Error(`the player name: ${playerName} is not registered`);
		}
		// update the player's game score
		this.gameMap[playerName] += 1;
		const player1Game = this.gameMap[this.player1Name];
		const player2Game = this.gameMap[this.player2Name];
		// update set winner
		if (player1Game >= 6 && player1Game - player2Game >= 2) {
			this.setScoreManager.updateTo(this.player1Name);
			this.gameCondition = GameConditionEnum.NONE;
			this.reset();
			return;
		}
		if (player2Game >= 6 && player2Game - player1Game >= 2) {
			this.setScoreManager.updateTo(this.player2Name);
			this.gameCondition = GameConditionEnum.NONE;
			this.reset();
			return;
		}
		// tie-break
		if (player1Game >= 6 && player2Game >= 6) {
			this.gameCondition = GameConditionEnum.TIEBREAK;
			return;
		}
	}
	/**
	 * return the game condition
	 * @returns
	 */
	getCondition(): GameConditionEnum {
		return this.gameCondition;
	}
	/**
	 * return the game scores
	 * @returns
	 */
	result(): string {
		const player1Game = this.gameMap[this.player1Name];
		const player2Game = this.gameMap[this.player2Name];
		return `${player1Game}-${player2Game}`;
	}
	/**
	 * reset the game score
	 */
	reset(): void {
		this.gameMap[this.player1Name] = 0;
		this.gameMap[this.player2Name] = 0;
		this.gameCondition = GameConditionEnum.NONE;
	}
}
