export enum PointConditionEnum {
	DEUCE = "DEUCE",
	P1AD = "P1AD",
	P2AD = "P2AD",
	NONE = "NONE",
}

export enum GameConditionEnum {
	TIEBREAK = "TIEBREAK",
	NONE = "NONE",
}

export enum SetConditionEnum {
	P1MATCH = "P1MATCH",
	P2MATCH = "P2MATCH",
	NONE = "NONE",
}
