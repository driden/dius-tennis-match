import { PointConditionEnum } from "./enum";
import IScore from "./score.manager.i";

export default interface IPointStoreManager extends IScore {
	getCondition(): PointConditionEnum;
}
