export default interface IMatch {
	pointWonBy(playerName: string): void;
	score(): void;
}
