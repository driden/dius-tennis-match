import { PointConditionEnum, SetConditionEnum } from "./enum";
import IScore from "./score.manager.i";

export default interface ISetScoreManager extends IScore {
	getCondition(): SetConditionEnum;
}
