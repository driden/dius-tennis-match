export default interface IScore {
	reset(): void;
	result(): string;
	updateTo(playerName: string): void;
}
