import { GameConditionEnum } from "./enum";
import IScore from "./score.manager.i";

export default interface IGameScoreManager extends IScore {
	getCondition(): GameConditionEnum;
}
